const express = require('express');
const app = express();
const port = 3000;
module.exports = app

// Define a route for the home page
app.get('/', (req, res) => {
  res.send('Hello, this is a Node.js web app!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://0.0.0.0:${port}`);
});
