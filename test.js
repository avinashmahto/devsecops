const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('/root/app');  // Assuming your main app file is named app.js
const expect = chai.expect;

chai.use(chaiHttp);

describe('Node.js App Tests', () => {
  it('should respond with the home page message', async () => {
    const res = await chai.request(app).get('/');
    expect(res).to.have.status(200);
    expect(res.text).to.equal('Hello, this is a Node.js web app!');
  });

  it('should start the server successfully', async () => {
    const res = await chai.request(app).get('/');
    expect(res).to.have.status(200);
    expect(res.text).to.equal('Hello, this is a Node.js web app!');
  });


  it('should respond with 404 for an invalid route', async () => {
    const res = await chai.request(app).get('/invalid');
    expect(res).to.have.status(404);
  });


  // Add more test cases as needed

  after(() => {
    // Additional cleanup or shutdown logic if needed
    process.exit(0);
  });
});
